package binarysearch2;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class BinarySearch2Test {

    private BinarySearch2 bSearch;

    @Before
    public void setUp() {
        bSearch = new BinarySearch2();
    }

    @Test(expected = IllegalArgumentException.class)
    public void should_throwIllegalArgumentException_when_sortedNumbersIsNull() {
        // given
        double[] sortedNumbers = null;
        double toFind = 0.0;

        // when
        bSearch.search(sortedNumbers, toFind);

        // then
        assert false;
    }

    @Test
    public void should_returnNegativeValue_when_sortedNumbersIsEmpty() {
        // given
        double[] sortedNumbers = {};
        double toFind = 0.0;

        // when
        int resultId = bSearch.search(sortedNumbers, toFind);
        int expectedId = -1;

        // then
        assertEquals(expectedId, resultId);
    }

    @Test
    public void should_returnCorrectId_when_numberToFindIsInOneElemSortedNumbers() {
        // given
        double[] sortedNumbers = {1};
        double toFind = 1;

        // when
        int resultId = bSearch.search(sortedNumbers, toFind);
        int expectedId = 0;

        // then
        assertEquals(expectedId, resultId);
    }

    @Test
    public void should_returnCorrectId_when_numberToFindIsMiddleNumberOfSortedNumebrs() {
        // given
        double[] sortedNumber = {1, 2, 3, 4, 5};
        double toFind = 3;

        // when
        int resultId = bSearch.search(sortedNumber, toFind);
        int expectedId = 2;

        // then
        assertEquals(expectedId, resultId);
    }

}
