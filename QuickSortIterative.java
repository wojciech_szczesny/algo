package aisd.lab2.sortcomparison;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class QuickSortIterative implements SortingAlgorithm {

    @Override
    public double[] sort(double[] unsortedVector) {
        if (unsortedVector == null) {
            throw new IllegalArgumentException("Input data cannot be null");
        }

        double[] vectorToSort = unsortedVector.clone();

        quicksort(vectorToSort);

        return vectorToSort;
    }

    private void quicksort(double[] data) {
        List<Integer> starts = new ArrayList<>();
        List<Integer> ends = new ArrayList<>();

        Integer left = 0;
        Integer right = data.length - 1;

        starts.add(left);
        ends.add(right);

        int n = 1;
        int pivot;

        if (left < right) {

            while (n > 0) {
                n--;
                left = starts.get(0);
                right = ends.get(0);
                System.out.println("Pobiera left: " + starts.get(0));
                System.out.println("Pobiera right: " + ends.get(0));
                pivot = splitData2(data, left, right);
                starts.remove(0);
                ends.remove(0);
                //System.out.println(pivot);
           
                if (pivot - 1 > left) {
                    System.out.println("Dodaje lewy: " + left + " " + (pivot - 1));
                    starts.add(left);
                    ends.add(pivot - 1);
                    n++;
                }

                if (pivot + 1 < right) {
                    System.out.println("Dodaje prawy: " + (pivot+1) + " " + right);
                    starts.add(pivot + 1);
                    ends.add(right);
                    n++;
                }
            }
        }
    }

    private int splitData(double[] data, int start, int end) {
        int left = start + 1;
        int right = end;

        while (left < right) {
            while (left < right && data[left] < data[start]) {
                left++;
            }

            while (left < right && data[right] >= data[start]) {
                right--;
            }

            swap(data, left, right);
        }

        if (data[left] >= data[start]) {
            left--;
        }

        swap(data, start, left);

        return left;
    }
    
    private int getIndexOfMiddleValue(double[] data, int a, int b, int c){
        if (data[a] > data[b]) {
            if (data[b] > data[c]) {
                return b;
            } else if (data[a] > data[c]) {
                return c;
            } else {
                return a;
            }
        } else {
            if (data[a] > data[c]) {
                return a;
            } else if (data[b] > data[c]) {
                return c;
            } else {
                return b;
            }
        }
    }
    
    private int splitData2(double[] data, int inStart, int inEnd) {
        int rnd = new Random().nextInt(inEnd-inStart+1)+inStart;
                    
        int start = getIndexOfMiddleValue(data, inStart, inEnd, rnd);
        System.out.println("Wylosowano index: " + rnd + "Wartosc: " + data[rnd]);
        System.out.println("Wylosowano index start: " + start + "Wartosc: " + data[start]);
        //System.out.println("Wartosc: " + data[rnd]);
        //System.out.println("index srodkowej wartosci: " + start);
        //System.out.println("Wartość srodkowej wartosci:" + data[start]);
        
        int left = inStart;
        int right = inEnd;

        while (left < right) {
            while (left < right && data[left] < data[start]) {
                left++;
            }
            while (left < right && data[right] >= data[start]) {
                right--;
            }
            System.out.println("UWAGA ZMIANA: " + left + " " + right);
            swap(data, left, right);
            System.out.println("Po zamianie: ");
            for(int i=0; i<data.length;i++){
            System.out.println(data[i]);
            }
            //System.out.println("Aktualne wartosci" + data[left] + " " + data[right]);
        }

        if (data[left] >= data[start]) {
            left--;
        }
        //System.out.println("UWAGA ZMIANA: " + start+ " " + left);
        //swap(data, start, left);
        System.out.println("Po zamianie po while: ");
        for(int i=0; i<data.length;i++){
            System.out.println(data[i]);
        }
        //System.out.println(start + " " + left);

        return left;
    }
    
    private void swap(double[] data, int firstId, int secondId) {
        if (firstId != secondId) {
            double firstValue = data[firstId];
            data[firstId] = data[secondId];
            data[secondId] = firstValue;
        }
    }

}
