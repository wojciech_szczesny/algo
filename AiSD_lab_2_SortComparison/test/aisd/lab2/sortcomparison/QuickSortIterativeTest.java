package aisd.lab2.sortcomparison;

import java.util.Arrays;
import java.util.Random;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class QuickSortIterativeTest {

    /*
    @Test(expected = IllegalArgumentException.class)
    public void should_throwIllegalArgumentException_when_sortedNumbersIsNull() {
        // given
        double[] sortedNumbers = null;

        // when
        QuickSortIterative mySort = new QuickSortIterative();
        mySort.sort(sortedNumbers);

        // then
        assert false;
    }
*/
    
    
    
    @Test
    public void randomTestSort() {
        
        //given
        double[] unsortedVector = { 3.0, 8.0, 0.0, 9.0, 11.0, 1.0, 13.0 };
        double[] expectedResult = { 0.0, 1.0, 3.0, 8.0, 9.0, 11.0, 13.0 };
        QuickSortIterative mySort = new QuickSortIterative();
        double[] result;
        
        //when
        result = mySort.sort(unsortedVector);
        
        //then
        assertTrue(Arrays.equals(expectedResult, result));
    
    }
/*    
    @Test
    public void bigRandomTestSort() {
        
        //given
        int numberOfElements = 10000;
        double[] unsortedVector = new double[numberOfElements];
        QuickSortIterative mySort = new QuickSortIterative();
        Random random = new Random();
        random.setSeed(40);
        for ( int i = 0; i < numberOfElements; i++){
            unsortedVector[i] = Math.abs(random.nextDouble());
        }
        double[] expectedResult = unsortedVector.clone();
        Arrays.sort(expectedResult);
        
        //when
        double[] result = mySort.sort(unsortedVector);
        
        //then
        assertTrue(Arrays.equals(expectedResult, result));
    
    }
    
    
    @Test
    public void pesimisticTestSort() {
        
        //given
        double[] unsortedVector = { 7.0, 6.0, 5.0, 4.0, 3.0, 2.0, 1.0};
        double[] expectedResult = { 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0};
        QuickSortIterative mySort = new QuickSortIterative();
        
        //when
        double[] result = mySort.sort(unsortedVector);
        
        //then
        assertTrue(Arrays.equals(expectedResult, result));
    
    }
    
    @Test
    public void BigPesimisticTestSort() {
        
        //given
        QuickSortIterative mySort = new QuickSortIterative();
        int numberOfElements = 50000;
        double[] unsortedVector = new double[numberOfElements];
        double[] expectedResult = new double[numberOfElements];
        for(int i = numberOfElements-1; i > 0; i--){
            unsortedVector[numberOfElements-i] = (double) i;
        }
        for (int i = 0; i < numberOfElements; i++) {
            expectedResult[i] = (double) i;
        }
        
        //when
        double[] result = mySort.sort(unsortedVector);
        
        //then
        assertTrue(Arrays.equals(expectedResult, result));
    
    }
        
    @Test
    public void optimisticTestSort() {
        
        //given
        double[] unsortedVector = { 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0};
        double[] expectedResult = { 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0};
        QuickSortIterative mySort = new QuickSortIterative();
        
        //when
        double[] result = mySort.sort(unsortedVector);    
        
        //then
        assertTrue(Arrays.equals(expectedResult, result));
    
    }
    
    @Test
    public void BigOptimisticTestSort() {
        
        //given
        double[] unsortedVector = new double[10000];
        double[] expectedResult = new double[10000];
        QuickSortIterative mySort = new QuickSortIterative();
        for (int i = 0; i < 10000; i++) {
            unsortedVector[i] = (double) i;
            expectedResult[i] = (double) i;
        }
        
        //when
        double[] result = mySort.sort(unsortedVector);
 
           
        //then
        assertTrue(Arrays.equals(expectedResult, result));
    
    }
    */
}
