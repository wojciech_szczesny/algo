package aisd.lab2.sortcomparison;

import java.util.Arrays;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class QuickSortRecursiveTest {
    @Test
    public void oTestSort() {
        
        //given
        double[] unsortedVector = { 3.0, 8.0, 1.0, 0.0, 9.0, 4.0};
        double[] expectedResult = { 0.0, 1.0, 3.0, 4.0, 8.0, 9.0};
        QuickSortRecursive mySort = new QuickSortRecursive();
        
        //when
        double[] result = mySort.sort(unsortedVector);
        
        //then
        assertTrue(Arrays.equals(expectedResult, result));
    
    }
    /*
    @Test
    public void optymisticTestSort() {
        
        //given
        double[] unsortedVector = { 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0};
        double[] expectedResult = { 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0};
        QuickSortRecursive mySort = new QuickSortRecursive();
        
        //when
        double[] result = mySort.sort(unsortedVector);
        
        //then
        assertTrue(Arrays.equals(expectedResult, result));
    
    }
    @Test
    public void pesimisticTestSort() {
        
        //given
        double[] unsortedVector = { 7.0, 6.0, 5.0, 4.0, 3.0, 2.0, 1.0};
        double[] expectedResult = { 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0};
        QuickSortRecursive mySort = new QuickSortRecursive();
        
        //when
        double[] result = mySort.sort(unsortedVector);
        
        //then
        assertTrue(Arrays.equals(expectedResult, result));
    
    }
    
    @Test
    public void randomTestSort() {
        
        //given
        double[] unsortedVector = { 3.0, 8.0, 1.0, 0.0, 9.0, 4.0, 11.0, 13.0, 44.0, 7.0};
        double[] expectedResult = { 0.0, 1.0, 3.0, 4.0, 7.0, 8.0, 9.0, 11.0, 13.0, 44.0};
        QuickSortRecursive mySort = new QuickSortRecursive();
        
        //when
        double[] result = mySort.sort(unsortedVector);
        
        //then
        assertTrue(Arrays.equals(expectedResult, result));
    
    }
*/
    
}
