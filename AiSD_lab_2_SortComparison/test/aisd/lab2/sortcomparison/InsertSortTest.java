package aisd.lab2.sortcomparison;

import java.util.Arrays;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;


public class InsertSortTest {
    
    @Test(expected = IllegalArgumentException.class)
    public void should_throwIllegalArgumentException_when_sortedNumbersIsNull() {
        
        // given
        double[] sortedNumbers = null;
        InsertSort mySort = new InsertSort();

        // when
        mySort.sort(sortedNumbers);

        // then
        assert false;
    }
    
    
    @Test
    public void bigOptymisticTestSort() {
        
        //given
        double[] unsortedVector = new double[10000];
        double[] expectedResult = new double[10000];
        InsertSort mySort = new InsertSort();
        double[] result;
        
        for (int i = 0; i < 10000; i++) {
            unsortedVector[i] = (double) i;
            expectedResult[i] = (double) i;
        }
        
        //when
        result = mySort.sort(unsortedVector);
        
        //then
        assertTrue(Arrays.equals(expectedResult, result));
        
    }
    
    @Test
    public void optymisticTestSort() {
        
        //given
        double[] unsortedVector = new double[10];
        double[] expectedResult = new double[10];
        InsertSort mySort = new InsertSort();
        double[] result;
        
        for (int i = 0; i < 10; i++) {
            unsortedVector[i] = (double) i;
            expectedResult[i] = (double) i;
        }
        
        //when
        result = mySort.sort(unsortedVector);
 
        //then
        assertTrue(Arrays.equals(expectedResult, result));     
    }
 
    @Test
    public void pesimisticTestSort() {
        
        //given
        double[] unsortedVector = { 9.0, 7.0, 5.0, 3.0, 2.0, 1.0 };
        double[] expectedResult = { 1.0, 2.0, 3.0, 5.0, 7.0, 9.0 };
        InsertSort mySort = new InsertSort();
        double[] result;
        
        //when
        result = mySort.sort(unsortedVector);
        
        //then
        assertTrue(Arrays.equals(expectedResult, result));
        
    }
    
    @Test
    public void bigPesimisticTestSort() {
        
        //given
        int numberOfElements = 50000;
        double[] unsortedVector = new double[numberOfElements];
        double[] expectedResult = new double[numberOfElements];
        InsertSort mySort = new InsertSort();
        double[] result;
        
        for(int i = numberOfElements-1; i > 0; i--){
            unsortedVector[numberOfElements-i] = (double) i;
        }
        
        for (int i = 0; i < numberOfElements; i++) {
            expectedResult[i] = (double) i;
        }
        
        //when
        result = mySort.sort(unsortedVector);
        
        //then
        assertTrue(Arrays.equals(expectedResult, result));
        
    }
    
    @Test
    public void randomTestSort() {
        
        //given
        double[] unsortedVector = { 1.0, 3.0, 7.0, 2.0, 5.0, 9.0 };
        double[] expectedResult = { 1.0, 2.0, 3.0, 5.0, 7.0, 9.0 };
        InsertSort mySort = new InsertSort();
        double[] result;
        
        //when
        result = mySort.sort(unsortedVector);
        
        //then
        assertTrue(Arrays.equals(expectedResult, result));
        
    }
    
}
