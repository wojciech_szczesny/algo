package aisd.lab2.sortcomparison;

public class InsertSort implements SortingAlgorithm {

    @Override
    public double[] sort(double[] unsortedVector) {
        if (unsortedVector == null) {
            throw new IllegalArgumentException("Input data cannot be null.");
        }

        int n = unsortedVector.length;
        int j;

        double[] vectorToSort = unsortedVector.clone();

        for (int i = 1; i < n; i++) {
            double tmpValue = vectorToSort[i];
            j = i - 1;

            while (j >= 0 && vectorToSort[j] > tmpValue) {
                vectorToSort[j + 1] = vectorToSort[j];
                j--;
            }
            vectorToSort[j + 1] = tmpValue;
        }

        return vectorToSort;
    }

}
