package aisd.lab2.sortcomparison;

import java.util.Random;

public class Timer {
    
    private final Random random;
    
    public Timer(){
        random = new Random();
    }
    
    public long[] getTimeOpt( int numberOfElements){
        
        double[] unsortedVector = new double[numberOfElements];
        long[] timeVector = new long[10];
        InsertSort mySort = new InsertSort();
        long suma = 0;
        
        for(int i = 0; i < numberOfElements; i++){
            unsortedVector[i] = (double) i;
        }
        
        for(int i = 0; i < 10; i++){
            long poczatek = System.nanoTime();
            mySort.sort(unsortedVector);
            timeVector[i] = System.nanoTime() - poczatek;
        }
        
        for(int i = 0; i < 10; i++){
            suma = suma + timeVector[i];   
        }
        
        System.out.println(suma/10);
        return timeVector;
          
    }
    public long[] getTimePes( int numberOfElements){
        
        double[] unsortedVector = new double[numberOfElements];
        long[] timeVector = new long[10];
        InsertSort mySort = new InsertSort();
        long suma = 0;
        
        for(int i = numberOfElements - 1; i > 0; i--){
            unsortedVector[numberOfElements - i] = (double) i;
        }
        
        for(int i = 0; i < 10; i++){
            long poczatek = System.nanoTime();
            mySort.sort(unsortedVector);
            timeVector[i] = System.nanoTime() - poczatek;
        }
        
        for(int i = 0; i < 10; i++){
            suma = suma + timeVector[i];   
        }
        
        System.out.println(suma / 10);
        
        return timeVector;
          
    }
    public long[] getTimeRand( int numberOfElements){
        double[] unsortedVector = new double[numberOfElements];
        long[] timeVector = new long[10];
        InsertSort mySort = new InsertSort();
        long suma = 0;
        
        random.setSeed(40);
        for ( int i = 0; i < numberOfElements; i++){
            unsortedVector[i] = Math.abs(random.nextDouble());
        }

        for ( int i = 0; i < 10; i++){
            long poczatek = System.nanoTime();
            mySort.sort(unsortedVector);
            timeVector[i]=System.nanoTime() - poczatek;
        }
        
        for ( int i = 0; i < 10; i++){
            suma = suma + timeVector[i];   
        }
        
        System.out.println(suma / 10);
        
        return timeVector;
          
    }
    public long[] getTimeOptQuick( int numberOfElements){
        
        double[] unsortedVector = new double[numberOfElements];
        long[] timeVector = new long[10];
        QuickSortIterative mySort = new QuickSortIterative();
        long suma = 0;
        
        for(int i = 0; i < numberOfElements; i++){
            unsortedVector[i] = (double) i;
        }
        
        for(int i = 0; i < 10; i++){
            long poczatek = System.nanoTime();
            mySort.sort(unsortedVector);
            timeVector[i]=System.nanoTime() - poczatek;
        }
        
        for(int i = 0; i < 10; i++){
            suma = suma + timeVector[i];   
        }
        
        System.out.println(suma / 10);
        
        return timeVector;        
    }
    public long[] getTimePesQuick( int numberOfElements){
        
        double[] unsortedVector = new double[numberOfElements];
        long[] timeVector = new long[10];
        QuickSortIterative mySort = new QuickSortIterative();
        long suma = 0;
        
        for(int i = numberOfElements - 1; i > 0; i--){
            unsortedVector[numberOfElements - i] = (double) i;
        }
        for(int i = 0; i < 10; i++){
            long poczatek = System.nanoTime();
            mySort.sort(unsortedVector);
            timeVector[i] = System.nanoTime() - poczatek;
        }
        
        for(int i = 0; i < 10; i++){
            suma = suma + timeVector[i];   
        }
        
        System.out.println(suma / 10);
        
        return timeVector;
          
    }
    public long[] getTimeRandQuick( int numberOfElements){
        
        double[] unsortedVector = new double[numberOfElements];
        long[] timeVector = new long[10];
        InsertSort mySort = new InsertSort();
        long suma = 0;
        
        random.setSeed(40);
        
        for ( int i = 0; i < numberOfElements; i++){
            unsortedVector[i] = Math.abs(random.nextDouble());
        }

        for ( int i = 0; i < 10; i++){
            long poczatek = System.nanoTime();
            mySort.sort(unsortedVector);
            timeVector[i] = System.nanoTime() - poczatek;
        }
        
        for ( int i = 0; i < 10; i++){
            suma = suma + timeVector[i];   
        }
        
        System.out.println(suma / 10);
        return timeVector;
          
    }
    
    
     public static void main(String args[]) {
         
         Timer timer = new Timer();
         int numberOfElem = 200000;
         //timer.getTimeOpt(numberOfElem);
         //timer.getTimePes(numberOfElem);
         //timer.getTimeRand(numberOfElem);
         //timer.getTimeOptQuick(numberOfElem);
         //timer.getTimePesQuick(numberOfElem);
         timer.getTimeRandQuick(numberOfElem);
         
     }
    

    
}
